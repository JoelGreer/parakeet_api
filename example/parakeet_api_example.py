import os

import argparse
import yaml

import numpy as np
import parakeet
from parakeet import config

def setup_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--pdb",
        type=str,
        help=(
            "Path to your pdb file"
        )
    )
    parser.add_argument(
        "--mrc",
        type=str,
        help=(
            "Path and name of the image are creating (should end with .mrc)"
        )
    )
    parser.add_argument(
        "--voltage",
        type=float,
        help="beam voltage (kV)",
        default=300.,
        required=False,
    )
    parser.add_argument(
        "--dose",
        type=float,
        help="Image e dosage (e/A^2)",
        default=45.,
        required=False,
    )
    parser.add_argument(
        "--box_x",
        type=int,
        help="Simulated box size along x axis",
        default=500,
        required=False,
    )
    parser.add_argument(
        "--box_y",
        type=int,
        help="Simulated box size along y axis",
        default=500,
        required=False,
    )
    parser.add_argument(
        "--box_z",
        type=int,
        help="Simulated box size along z axis",
        default=500,
        required=False,
    )
    parser.add_argument(
        "--pixel_size",
        type=float,
        help="pixel size (A)",
        default=1.0,
        required=False,
    )
    parser.add_argument(
        "--mol_x",
        type=float,
        help="x position of molecule",
        default=250,
        required=False,
    )
    parser.add_argument(
        "--mol_y",
        type=float,
        help="y position of molecule",
        default=250,
        required=False,
    )
    parser.add_argument(
        "--mol_z",
        type=float,
        help="z position of molecule",
        default=250,
        required=False,
    )
    parser.add_argument(
        "--defocus",
        type=float,
        help="defocus (-ve for underfocus) (A)",
        default=-20000,
        required=False,
    )
    parser.add_argument(
        "--phi",
        type=float,
        help="phi rot of molecule",
        default=0,
        required=False,
    )
    parser.add_argument(
        "--theta",
        type=float,
        help="theta rot of molecule",
        default=0,
        required=False,
    )
    parser.add_argument(
        "--psi",
        type=float,
        help="psi rot of molecule",
        default=0,
        required=False,
    )
    parser.add_argument(
        "--config_file",
        type=str,
        help="Config file to create (for future reference of parameters or repetition of simulation)",
        default="config.yaml",
        required=False,
    )
    parser.add_argument(
        "--cpugpu",
        type=str,
        default="gpu",
        required=True,
    )

    parser.add_argument(
        "--ice",
        type=bool,
        default=False,
        required=False,
    )
    return parser

def set_config(args):
    parakeet_config = config.new(filename=args.config_file, full=True)
    parakeet_config.microscope.beam.electrons_per_angstrom = args.dose
    parakeet_config.microscope.beam.energy = args.voltage
    parakeet_config.microscope.detector.nx = args.box_x
    parakeet_config.microscope.detector.ny = args.box_y
    parakeet_config.microscope.detector.pixel_size = args.pixel_size
    parakeet_config.microscope.lens.c_10 = args.defocus
    parakeet_config.sample.box = [args.box_x, args.box_y, args.box_z]
    parakeet_config.sample.centre = [args.box_x/2, args.box_y/2, args.box_z/2]
    parakeet_config.sample.shape.type = "cuboid"
    parakeet_config.sample.shape.cuboid.length_x = args.box_x * args.pixel_size
    parakeet_config.sample.shape.cuboid.length_y = args.box_y * args.pixel_size
    parakeet_config.sample.shape.cuboid.length_z = args.box_z * args.pixel_size
    parakeet_config.simulation.ice = args.ice
    
    parakeet_config.sample.molecules = config.Molecules()
    local_molecules = []
    molecule_poses = []
    single_pose = {
        "position": (args.mol_x, args.mol_y, args.mol_z),
        "orientation": (args.phi, args.theta, args.psi),
    }
    molecule_poses.append(single_pose)

    single_molecule = {"filename": args.pdb, "instances": molecule_poses}
    local_molecules.append(single_molecule)
    parakeet_config.sample.molecules.local = local_molecules

    # save the config to file for reference and for running simulation
    with open (args.config_file, "w") as config_file:
        yaml.safe_dump(parakeet_config.dict(), config_file)

    return parakeet_config

def run_sim(args):
    """
    parakeet.sample.new(args.config_file, "sample.h5")
    parakeet.sample.add_molecules(args.config_file, "sample.h5")
    parakeet.simulate.exit_wave(args.config_file, "sample.h5", exit_wave_file="exit_wave.h5")
    parakeet.simulate.optics(args.config_file, exit_wave_file="exit_wave.h5", optics_file="optics.h5")
    parakeet.simulate.image(args.config_file, optics_file="optics.h5", image_file="image.h5")
    """
    
    os.system("parakeet.sample.new -c {} -s sample.h5".format(args.config_file))
    os.system("parakeet.sample.add_molecules -c {} -s sample.h5".format(args.config_file))
    os.system("parakeet.simulate.exit_wave -c {} -s sample.h5 -e exit_wave.h5 -d {}".format(args.config_file, args.cpugpu))
    os.system("parakeet.simulate.optics -c {} -e exit_wave.h5 -d {}".format(args.config_file, args.cpugpu))
    os.system("parakeet.simulate.image -c {} -o optics.h5 -i image.h5".format(args.config_file))

    os.system("parakeet.export image.h5 -o {}".format(args.mrc))
    os.system("rm sample.h5 exit_wave.h5 optics.h5 image.h5")

def run_main(args):
    config = set_config(args)
    run_sim(args)
    return

if __name__ == "__main__":

    parser = setup_args()
    args = parser.parse_args()
    run_main(args)