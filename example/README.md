# Parakeet API Example

`parakeet_api_example.py` is a Python (>3.8) script which will allow you to run a simple Parakeet simulation from the `bash` command line via:
`bash run_parakeet_api.sh`
This is a shell script which you can use to provide the command line arguments to the python script and run it instead of having to manually type them out every time.

## Prerequisites
In order to use the python script, you will need:
* Python >3.8
I suggest using miniconda or pip3 to set up and activate a python environment to work in.

You will also need to install these (non-standard) Python packages:
* pyyaml
* numpy
* parakeet
Your python version should come with the other req packages:
* os
* argparse

## CMD line args
These are the values parsed from the command line and passed to the argument parser python class.
For this script, these values are mostly to do with setting parameters in the parakeet config file:
* voltage
* dose
* box_?
* pixel_size
* mol_?
* defocus
* phi
* theta
* psi
* cpugpu
The last of these sets whether to use `gpu` or `cpu`. Choose `gpu` if you have it.

Other args are:
* pdb - the pdb file defining the molecule you want to simulate
* mrc - the name to give the image file you will generate 
* config_file - the filename to give the configuration yaml file for your simulation. This is use  for the simulation and can also be read (using a text editor or as a python dictionary via pyyaml) later. 
Recommended to make the mrc and the config file names correspond to each other in a sensible way. I often use something like image_000000.mrc and config_000000.yaml so the relations are explicit for each pair of files. 


## Mrc Format
Here's info on the .mrc format: <https://mrcfile.readthedocs.io/en/stable/usage_guide.html>. I recommend installing the python package napari to look at .mrc files. To open an mrc file with Napari, you'll also need to install this package <https://github.com/alisterburt/napari-mrcfile-reader>.


## Extending the Script
If you want to make a lot of different images from many different pdb files, I would recommend:
* putting the pdb files in a directory
* Using python package glob2, calling via something like `glob.glob(pdb_files_path)` to get a list of pdb files
* Looping over this list to creating the corresponding yaml and image via Parakeet for each, ensuring that each yaml and mrc have a unique and associated name (something like `for i, pdb in enumerate(pdb_list)` will give you an index, `i` and the pdb file.)

If you want to move the pdb in the image, alter `mol_x`, `mol_y`, `mol_z`.
If you want to rotate the pdb in the image, alter `phi`, `theta`, `psi`. The rotations Euler angles are intrinsic, right handed rotations around ZYZ.


### Support
Please direct questions to joel.greer@stfc.ac.uk 